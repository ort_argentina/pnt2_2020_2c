
export default {
  estadoCompras: (state) => {
    let comprado = 0
    let faltaComprar = 0
    
    state.listaCompras.foreach(compra => {
      if (compra.comprado)
        comprado++
      else
        faltaComprar++
    })
    return { comprado, faltaComprar }
  }
}