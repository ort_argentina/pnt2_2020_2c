import * as types from './types'

export default {
  [types.AGREGAR_PRODUCTO]: function (state, data) {
    state.listaCompras.push(data)
  },
  [types.MODIFICAR_PRODUCTO]: function (state, data) {
    let i = 0
    state.listaCompras[i] = data
  },
  [types.ELIMINAR_PRODUCTO]: function (state, id) {
    state.listaCompras.pop(id)
  },
  [types.MARCAR_COMPRA]: function (state, { id, comprado }) {
    let i = id
    state.listaCompras[i].comprado = comprado
  }
}