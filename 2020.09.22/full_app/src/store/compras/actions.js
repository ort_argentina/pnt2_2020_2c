import * as types from './types'

export default {
  [types.AGREGAR_PRODUCTO]: function ({ commit, state }, data) {
    console.log(state)
    commit(types.AGREGAR_PRODUCTO, data)
  },
  [types.MODIFICAR_PRODUCTO]: function ({ commit, state }, data) {
    console.log(state)
    commit(types.MODIFICAR_PRODUCTO, data)
  },
  [types.ELIMINAR_PRODUCTO]: function ({ commit, state }, id) {
    console.log(state)
    commit(types.ELIMINAR_PRODUCTO, id)
  },
  [types.MARCAR_COMPRA]: function ({ commit, state }, { id, comprado }) {
    console.log(state)
    commit(types.MARCAR_COMPRA, { id, comprado })
  }
}