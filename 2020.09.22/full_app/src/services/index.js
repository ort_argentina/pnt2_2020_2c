export const leerNegocio = () => {
  // En realidad va un fetch/axios
  return [
    {
      value: 1,
      label: 'Farmacia 1'
    },
    {
      value: 2,
      label: 'Almacen Don Pepe'
    },
    {
      value: 3,
      label: 'Verduleria Doña Juana'
    },
    {
      value: 4,
      label: 'Veterinaria Pichichus'
    }
  ]
}
