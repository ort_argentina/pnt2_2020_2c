import { connect } from 'react-redux'
import React, { Component } from 'react'
import { addMateria, updateInstituto } from '../store/actions'

const mapStateToProps = (state) => {
  return {
    propiedadMaterias: state.materias,
    propiedadInstituto: state.instituto
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    propiedadAddMateria: materia => dispatch(addMateria(materia)),
    propiedadUpdateIntitituto: nombreInstituto => dispatch(updateInstituto(nombreInstituto))
  }
}

class ListComponent extends Component {
  constructor() {
    super()

    this.idMateria = React.createRef();
    this.nombreMateria = React.createRef();

    this.nombreInstituto = React.createRef();

    this.agregarMateria = this.agregarMateria.bind(this)
  }

  agregarMateria(event) {
    this.props.propiedadAddMateria({
      id: this.idMateria.current.value,
      nombre: this.nombreMateria.current.value
    })
  }

  actualizarInstituto(event) {
    if (event.key === 'Enter') {
      this.props.propiedadUpdateIntitituto(this.nombreInstituto.current.value)
    }
  }

  render() {
    return (
      <div>
        <h3>{this.props.propiedadInstituto}</h3>
        <input type="text" id="nombreInstituto" 
          ref={this.nombreInstituto} 
          onKeyPress={ (event) => this.actualizarInstituto(event) }
        /> <br />
        <hr />
        <input type="text" id="id" ref={this.idMateria} /> <br />
        <input type="text" id="nombre" ref={this.nombreMateria} /> <br />
        <button id="agregarMateria" onKeyPress={ this.agregarMateria }>Agregar</button>
        <hr />
        <ul>
          {
            this.props.propiedadMaterias.map(el => (
              <li key={el.id}>
                {el.nombre}
              </li>
            ))
          }
        </ul>
      </div>
    )
  }
}

const List = connect(mapStateToProps, mapDispatchToProps)(ListComponent)

export default List
