import { connect } from 'react-redux'
import React from 'react'

const mapStateToProps = (state) => {
  return {
    materias: state.materias
  }
}

const ConnectList = ({materias}) => (
  <div>
    <hr />
    {materias.length}
    <hr />
  </div>
)

const List = connect(mapStateToProps)(ConnectList) 

export default List