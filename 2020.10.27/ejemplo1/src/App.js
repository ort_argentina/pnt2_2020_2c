import React from 'react';

import './App.css';
import List from './components/List2';

class App extends React.Component {

  render() {
    return (
      <div className="App">
        <h2>React Redux</h2>
        <List />
      </div>
    )
  }
}

export default App;
