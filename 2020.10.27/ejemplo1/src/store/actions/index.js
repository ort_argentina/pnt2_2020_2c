import { ADD_MATERIA, UPDATE_INSTITUTO } from '../constants/action_types'

export function addMateria(data) {
  return {
    type: ADD_MATERIA,
    data
  }
}

export function updateInstituto(data) {
  return {
    type: UPDATE_INSTITUTO,
    data
  }
}
