import { ADD_MATERIA, UPDATE_INSTITUTO } from '../constants/action_types'

const initialState = {
  materias: [],
  instituto: '--'
}

function rootReducer(state=initialState, action) {

  switch (action.type) {
    case ADD_MATERIA:
      console.log('Agregar materia: ' + action.data)

      // state.materias.push(action.data)

      state = Object.assign({}, state , {
        materias: state.materias.concat(action.data)
      })
      break
  
    case UPDATE_INSTITUTO:
      console.log('Actualizar instituto: ' + action.data)
      // state.instituto = action.data

      state = Object.assign({}, state , {
        instituto: action.data
      })
      break
  }

  return state
}

export default rootReducer
