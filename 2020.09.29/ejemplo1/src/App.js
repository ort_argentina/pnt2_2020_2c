import React from 'react';
import { BrowserRouter, Link, Route } from 'react-router-dom'

import './App.css';
import Dos from './components/Dos';
import Tres from './components/Tres';
import Uno from './components/Uno'


class App extends React.Component {

  render() {
    return (
      <div className="App">
        <header className="App-header">
          {/* <Uno />
          <Dos />
          <Tres prop1="1234" sarasa="abcde" /> */}

          <hr />
          <hr />
          
          <BrowserRouter>
            <Link to='/'>Ir a Comp. Uno</Link>
            <Link to='/dos'>Ir a Comp. Dos</Link>
            <Link to='/tres'>Ir a Comp. Tres</Link>

            <Route exact path='/' component={Uno}/>
            <Route path='/dos' component={Dos}/>
            <Route exact path='/tres' component={Tres}/>
            <Route path='/tres/uno' component={Uno}/>

          </BrowserRouter>

        </header>
      </div>
    )
  }
}

export default App;
