import React from 'react';

class Dos extends React.Component {

  constructor() {
    super()

    this.state = {
      var1: '1',
      var2: '2'
    }

    this.textoVar1OnChangeHandler = this.textoVar1OnChangeHandler.bind(this)
  }

  componentDidUpdate (prevProps, prevState) {
    console.log('----')
    console.log(prevState)
    console.log('----')
  }
  
  textoVar1OnChangeHandler(args) {
    console.log(this.state.var1)
    // this.state.var1 = args.target.value   No funciona el render
    this.setState({
      var1: args.target.value
    })
    console.log(args.target.value)
  }

  render() {
    // this.state.var1 = 10

    let t = <div>
      <h2>Var 1: { this.state.var1 }</h2>
      <h2>Var 2: { this.state.var2 }</h2>
      <input type="text" id="textoVar1" onChange={ this.textoVar1OnChangeHandler } />
      <input type="text" id="textoVar1" onChange={ (args) => { this.setState({ var2: args.target.value }) }} />
    </div>

    return t
  }
}

export default Dos
