import React from 'react';

class Uno extends React.Component {

  constructor() {
    super()

    this.state = {
      var1: '1',
      var2: '2'
    }

    console.log('Construye')
  }

  componentDidMount () {
    console.log('Componente montado')
  }



  render() {
    console.log('Render')
    // this.state.var1 = 10

    let x = <h2>Titulo { this.state.var1 }</h2>
    let y = <h2>Titulo { this.state.var2 }</h2>
    
    let t = <div>{x}{y}</div>

    return t
  }
}

export default Uno
