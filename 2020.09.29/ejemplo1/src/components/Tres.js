import React from 'react';

class Tres extends React.Component {

  constructor(props) {
    super()

    this.state = {
      var1: props.prop1,
      var2: '2'
    }
  }

  render() {
    let t = <div>
      <h2>Var 1: { this.state.var1 }</h2>
      <h2>Var 2: { this.state.var2 }</h2>
      
      <h2>Prop 1: { this.props.prop1 }</h2>
      <h2>Prop Sarasa: { this.props.sarasa }</h2>

      <input type="text" id="textoVar1" onChange={ this.textoVar1OnChangeHandler } />
      <input type="text" id="textoVar1" onChange={ (args) => { this.setState({ var2: args.target.value }) }} />
    </div>

    return t
  }
}

export default Tres
