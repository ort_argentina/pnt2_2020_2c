// actions.js
import { ADD_LIKE, ADD_FRIEND } from '@/store/modules/user/types'

export default {
  [ADD_LIKE]: function ({ commit, state }) {
    console.log(state.likes)
    commit(ADD_LIKE) // Invocar a la mutacion 'addLike'
    console.log(state.likes)
  },
  [ADD_FRIEND]: function ({ commit, state }, payload) {
    console.log(state.likes)
    // Validar estado crediticio
    // si es bueno
    commit(ADD_FRIEND, payload) // Invocar a la mutacion 'addLike'
  },
}
