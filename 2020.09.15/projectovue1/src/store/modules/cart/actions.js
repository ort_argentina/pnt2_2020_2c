// actions.js
import { ADD_ITEM /*, MODIFY_ITEM, REMOVE_ITEM*/ } from '@/store/modules/cart/types'

export default {
  [ADD_ITEM]: function ({ commit, state }, payload) {
    console.log('en action')
    if (state.items[payload.id]) {
      state.items[payload.id].quantity += payload.order.quantity
      // commit(INCRAESE_QUANTITY, {id: payload.id, quantity: payload.order.quantity})
    } else
      commit(ADD_ITEM, payload)
  }
  // },
  // [MODIFY_ITEM]: function ({ commit, state }, payload) {

  // },
  // [REMOVE_ITEM]: function ({ commit, state }, payload) {

  // }
}
