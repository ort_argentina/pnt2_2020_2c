import { ADD_ITEM, MODIFY_ITEM, REMOVE_ITEM } from '@/store/modules/cart/types'

export default {
  [ADD_ITEM]: function (state, data) {
    console.log('en mutacion')
    state.items[data.id] = data.order
  },
  [MODIFY_ITEM]: function (state, data) {
    state.items[data.id] = data.order
  },
  [REMOVE_ITEM]: function (state, data) {
    delete state.items[data]
  }
}
