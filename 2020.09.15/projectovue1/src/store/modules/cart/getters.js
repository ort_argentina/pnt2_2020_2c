export default {
  total (state) {
    let t = 0

    Object.values(state.items).forEach(element => {
      t += element.price * element.quantity
    })

    return t
  }
}