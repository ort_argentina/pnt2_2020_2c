# pnt2_2020_2c

2020.08.04    Introduccion Javascript
2020.08.11    Javascript. Vue.JS Embebido
2020.08.18    Vue.JS Embebido
2020.08.25    Vue.JS Embebido.
2020.09.01    VueJS en Proyecto.
2020.09.08    VueJS - Store - Router
2020.09.15    VueJS - Store - Intro Quasar
2020.09.22    Quasar Full App
2020.09.29    React
2020.10.06    
2020.10.13
2020.10.20                - Entrega TP 1
2020.10.27    React Redux - TPs
2020.11.03    TPs
2020.11.10    TPs
2020.11.17    TPs         - Entrega TP 2

2020.11.24    Entrega Adicional TPs.


Temas adicionales:
- Docker: 
  - Hyper-V (BIOS)
  - Windows: Pro, Educ, Enter
  - WSL 2 (Windows Subsystem Linux) https://docs.microsoft.com/en-us/windows/wsl/install-win10


# Trabajos Practicos Cursadas

## TP1: Quasar con Vue
    - Storage, Router
    - Al menos 3 paginas (1 ABM, 1 Listado, 1 A elegir)

##  TP2: React
    - Storage, Router
    - Al menos 3 paginas (1 ABM, 1 Listado, 1 A elegir)
    - bootstrap-react


##  Consideracion es para la nota de los TP (Cursada):
    - Llegamos al 8 (ocho)
      - Modularizado  (Componentes, paquetes)
      - Nomenclatura  (Lint)  (Es correcta la nomenclatura con menos de 5 errores)
      - Utilizar lo que se pide en el enunciado.
      - El acoplamiento (Cero hardcoding)

    - Llegar al 9/10 (nueve/diez)
      - Sobrepasar los requerimientos.


# Examen Final
    - Defensa Oral de ambos TPs de la cursada
    - Llegan al 8
      - Respondiendo bien las preguntas
    
    - LLegan al 10
      - Si le agregan extras al TP1 o TP2. (Usar alguna api loca: google maps, clima)

    - Martes 1
    - Martes 15
