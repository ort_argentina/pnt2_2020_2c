
// Handler al evento fetch
self.addEventListener('fetch', event => {
  console.log(event)
  let url = new URL(event.request.url)
  if (url.pathname.startsWith('/test')) {
    event.respondWith(new Response('Hola Mundo ServiceWorker'))
  }
})
