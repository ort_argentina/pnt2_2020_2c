import Vue from 'vue'
import App from './App.vue'

import store from './store'
import router from './router'

Vue.config.productionTip = false

let app = new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app')

// let app = new Vue({
//   el: '#app',
//   store,
//   components: { App },
//   template: '<App/>'
// })

window.miapp = app
