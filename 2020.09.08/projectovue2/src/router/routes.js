
let routes = [
  {
    path: '/',
    name: 'raiz',
    component: () => import('@/pages/Home'),
    children: [
      {
        path: 'hijo1',
        name: 'raiz-hijo1',
        component: () => import('@/components/Hijo1')
      },
      {
        path: 'hijo2',
        name: 'raiz-hijo2',
        component: () => import('@/components/Hijo2')
      }
    ]
  },
  {
    path: '/about',
    name: 'about',
    component: () => import('@/pages/About')
  }
]

export default routes