import { LOGIN, ADD_FRIEND, ADD_LIKE } from '@/store/types'

export default {
  [LOGIN]: function (state, data) {
    state.username = data.username
  },
  [ADD_FRIEND]: function (state, data) {
    state.friends.push(data)
  },
  [ADD_LIKE]: function (state) {
    state.likes++
  }
}
