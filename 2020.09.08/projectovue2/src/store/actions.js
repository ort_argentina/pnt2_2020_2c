// actions.js
import { ADD_LIKE } from '@/store/types'

export default {
  [ADD_LIKE]: function ({ commit }) {
    commit(ADD_LIKE) // Invocar a la mutacion 'addLike'
  }
}
