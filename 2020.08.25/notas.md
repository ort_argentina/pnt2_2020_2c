# Creacion de proyecto para Vue

- Instalar el cliente de Vue:
```
npm install -g @vue/cli
```
- Crear un proyecto:
```
vue create nombreProyecto
```
- Ejecutar el proyecto:
```
npm run serve
```
